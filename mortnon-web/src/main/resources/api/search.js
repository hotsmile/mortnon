let api = [];
api.push({
    alias: 'DemoController',
    order: '1',
    link: 'demo',
    desc: 'Demo',
    list: []
})
api[0].list.push({
    order: '1',
    desc: 'hello world',
});
api[0].list.push({
    order: '2',
    desc: '表单提交demo',
});
api[0].list.push({
    order: '3',
    desc: '验证错误',
});
api.push({
    alias: 'HomeController',
    order: '2',
    link: '首页',
    desc: '首页',
    list: []
})
api[1].list.push({
    order: '1',
    desc: '跳转到mortnon首页',
});
api.push({
    alias: 'ApiController',
    order: '3',
    link: 'api工具',
    desc: 'api工具',
    list: []
})
api[2].list.push({
    order: '1',
    desc: '跳转到api页面',
});
api[2].list.push({
    order: '2',
    desc: '跳转到swagger页面',
});
api.push({
    alias: 'CaptchaController',
    order: '4',
    link: '验证码',
    desc: '验证码',
    list: []
})
api[3].list.push({
    order: '1',
    desc: '获取验证码',
});
api.push({
    alias: 'LoginController',
    order: '5',
    link: '登录',
    desc: '登录',
    list: []
})
api[4].list.push({
    order: '1',
    desc: '用户名密码登录',
});
api[4].list.push({
    order: '2',
    desc: '验证是否登录成功',
});
api[4].list.push({
    order: '3',
    desc: '获取登录用户信息',
});
api[4].list.push({
    order: '4',
    desc: '需要权限码',
});
api[4].list.push({
    order: '5',
    desc: '需要权限码Other',
});
api.push({
    alias: 'error',
    order: '6',
    link: 'error_code_list',
    desc: '错误码列表',
    list: []
})
api.push({
    alias: 'dict',
    order: '7',
    link: 'dict_list',
    desc: '数据字典',
    list: []
})
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    const theEvent = e;
    const code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
        const search = document.getElementById('search');
        const searchValue = search.value;
        let searchArr = [];
        for (let i = 0; i < api.length; i++) {
            let apiData = api[i];
            const desc = apiData.desc;
            if (desc.indexOf(searchValue) > -1) {
                searchArr.push({
                    order: apiData.order,
                    desc: apiData.desc,
                    link: apiData.link,
                    list: apiData.list
                });
            } else {
                let methodList = apiData.list || [];
                let methodListTemp = [];
                for (let j = 0; j < methodList.length; j++) {
                    const methodData = methodList[j];
                    const methodDesc = methodData.desc;
                    if (methodDesc.indexOf(searchValue) > -1) {
                        methodListTemp.push(methodData);
                        break;
                    }
                }
                if (methodListTemp.length > 0) {
                    const data = {
                        order: apiData.order,
                        desc: apiData.desc,
                        link: apiData.link,
                        list: methodListTemp
                    };
                    searchArr.push(data);
                }
            }
        }
        let html;
        if (searchValue == '') {
            const liClass = "";
            const display = "display: none";
            html = buildAccordion(api,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        } else {
            const liClass = "open";
            const display = "display: block";
            html = buildAccordion(searchArr,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        }
        const Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            const links = this.el.find('.dd');
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown);
        };
        Accordion.prototype.dropdown = function (e) {
            const $el = e.data.el;
            $this = $(this), $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp("20").parent().removeClass('open');
            }
        };
        new Accordion($('#accordion'), false);
    }
}

function buildAccordion(apiData, liClass, display) {
    let html = "";
    let doc;
    if (apiData.length > 0) {
        for (let j = 0; j < apiData.length; j++) {
            html += '<li class="'+liClass+'">';
            html += '<a class="dd" href="#_' + apiData[j].link + '">' + apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
            html += '<ul class="sectlevel2" style="'+display+'">';
            doc = apiData[j].list;
            for (let m = 0; m < doc.length; m++) {
                html += '<li><a href="#_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + doc[m].desc + '</a> </li>';
            }
            html += '</ul>';
            html += '</li>';
        }
    }
    return html;
}